const path                  = require('path');
const webpack               = require('webpack');
const ExtractTextPlugin     = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin     = require('html-webpack-plugin');
const CopyWebpackPlugin     = require('copy-webpack-plugin');
const BundleAnalyzerPlugin  = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const childProcess          = require('child_process');

const REVISION = childProcess.execSync('git rev-parse HEAD').toString();

module.exports = function (env) {
  let platform = (env && env.PLATFORM ) ? env.PLATFORM : 'web';
  let settings = {
    // platform | source folder       | outputPath
    'cordova' : ['./src/mobile/'      , 'cordova/www'   ],
    'electron': ['./src/desktop/'     , 'electron/dist' ],
    'web'     : ['./src/web/'         , 'www'           ]
  };
  let entry         = settings[platform][0]+'index.ts';
  let outputPath    = path.resolve(__dirname, settings[platform][1]);
  let htmlTemplate  = settings[platform][0]+'index.html';

  return {
    target: 'web',
    entry: entry,
    output: {
      filename: 'index.[hash:8].js',
      path: outputPath
    },
    module: {
      loaders: [
        // inject data
        { test: /\.json$/, loader: 'json-loader' },
        { test: /\.yaml$/, loader: 'yaml-loader' },
        // linters
        //{ test: /\.styl$/, loader: 'stylint-loader', exclude: /node_modules/, enforce: 'pre' },
        //{ test: /\.ts$/  , loader: 'tslint-loader' , exclude: /node_modules/, enforce: 'pre' },
        { test: /\.vue$/, loader: 'vue-loader' },
        // styles
        {test: /\.(css)$/,         loaders: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader?sourceMap' })},
        {test: /\.(styl|stylus)$/, loaders: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader?url=false!stylus-loader?sourceMap'})},
        //
        { test: /\.ts$/  , loader: 'ts-loader', options: { appendTsSuffixTo: [/\.vue$/] } },
        { test: /\.html$/, loader: 'html-loader' },
        { test: /\.pug$/ , loader: 'pug-loader' },
        // images
        { test: /\.(svg|png|jpe?g|gif)(\?\S*)?$/, loader: 'file-loader?name=assets/images/[name].[hash:7].[ext]' },
        // fonts
        { test: /\.(eot|woff|woff2|ttf)(\?\S*)?$/, loader: 'file-loader?name=assets/fonts/[name].[hash:7].[ext]' },
        // video and sounds
        { test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/, loader: 'file-loader?name=media/[name].[hash:7].[ext]' }
      ]
    },
    plugins: [
      // new BundleAnalyzerPlugin(),                      // https://github.com/th0r/webpack-bundle-analyzer
      new ExtractTextPlugin('styles.[hash:8].css'),       // extract styles to seperate file
      new HtmlWebpackPlugin({ template: htmlTemplate }),  // inject dependencies to html
      new CopyWebpackPlugin([                             // copy static files
        {from: 'src/assets', to: 'assets'}
      ]),
      new webpack.DefinePlugin({                          // inject global variables
        API_URI       : JSON.stringify(env ? env.API_URI      : ''),
        REDIRECT_URI  : JSON.stringify(env ? env.REDIRECT_URI : ''),
        SENTRY        : JSON.stringify(env ? env.SENTRY       : ''),
        INTERCOME     : JSON.stringify(env ? env.INTERCOME    : ''),
        APPCUES       : JSON.stringify(env ? env.APPCUES      : ''),
        REVISION      : JSON.stringify(platform+":"+REVISION)
      })
    ],
    resolve: {
      modules: ['src', 'node_modules', ],
      extensions: ['.js', '.ts', ],
      alias: {
        vue: 'vue/dist/vue.js'
      }
    },
    devServer: {
      contentBase: path.join(__dirname, "www"),
      //compress: true,
      port: 3000,
      hot: false,
      historyApiFallback: {
        disableDotRule: true
      }
    }
  };
};
