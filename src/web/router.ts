import Vue from 'vue'
import Router from 'vue-router'

import errorPage from './pages/error.vue'
import mainPage  from './pages/main.vue'


Vue.use(Router);


let router = new Router({
  mode: 'history',
  routes: [
    {	path: '/error'  , component: errorPage },
    { path: '/'       , component: mainPage  },
    {	path: '*'       , component: errorPage }
  ]
});

export default router;
