import './index.styl'
import Vue    from 'vue'
import router from './router'


var app = new Vue({ el: '#app', router, template: '<router-view></router-view>' });
