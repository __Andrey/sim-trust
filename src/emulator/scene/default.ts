export default {
  users: [
    { id: 1, name: '1', ai: 'default', links: [
      { user: '2', force: '2' },
    ]},
    { id: 2, name: '2', ai: 'default', links: [
      { user: '1', force: '2' }
    ]},
    { id: 3, name: '3', ai: 'default', links: [
      { user: '1', force: '2' }
    ]},
    { id: 4, name: '4', ai: 'default', links: [
      { user: '1', force: '2' }
    ]}
  ]
}
