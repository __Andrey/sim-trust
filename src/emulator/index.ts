import defaultScene from './scene/default'


class Emulator {

  users = {};
  step  = 0;

  constructor() {
    this.loadScene()
  }

  public loadScene(name?: string) {
    this.users = defaultScene.users;
  }

  public nextStep() {
    this.step = this.step + 1;
  }

  public prevStep() {
    this.step = this.step - 1;
  }
}

let emulator = new Emulator()
export default emulator
